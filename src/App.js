import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import ShowPages from "./components/ShowPages/ShowPages";
import EditPages from "./components/EditPages/EditPages";

const App = () => (
    <div className='App'>
        <BrowserRouter>
            <Layout/>
                <Switch>
                    <Route path='/pages/:title' component={ShowPages}/>
                    <Route path='/edit' component={EditPages}/>
                </Switch>

        </BrowserRouter>
    </div>

);

export default App;
