import React, {useState} from 'react';
import axios from "axios";

const ShowPages = ({match}) => {
    const page = match.params.title;

    const [pageContent, setPageContent] =useState([]);

    const fetchData = async ()=>{
        const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/pages/'+page+'.json');
        const pages = response.data;
        setPageContent(pages);
    };
    fetchData().catch(console.error);

    return (
        <div>
            <h3>{pageContent.title}</h3>
            <p>{pageContent.content}</p>
        </div>
    );
};

export default ShowPages;