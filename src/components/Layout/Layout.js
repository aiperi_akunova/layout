import React from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = () => {
    return (
        <header className='header'>
            <div>
                <h2>Our Website</h2>
            </div>
            <div className='navigation'>
                <NavLink to='/pages/about'>About</NavLink>
                <NavLink to='/pages/contacts'>Contact</NavLink>
                <NavLink to='/pages/news'>News</NavLink>
                <NavLink to='/pages/mission'>Mission</NavLink>
                <NavLink to='/edit'>Admin</NavLink>
            </div>
        </header>
    );
};

export default Layout;